#!/usr/bin/env python

"""Tool for managing tasks

   It is a small command line utility to create,
   list, remove, and edit tasks.
"""

# See https://manikos.github.io/a-tour-on-python-packaging

from setuptools import setup


setup(
    # -- Project information
    name="task",
    version="0.1.0",
    author="Thomas Schraitle",
    author_email="toms@suse.de",
    url="http://bitbucket.org/tomschr/task",
    license="MIT license",

    # -- Description
    description=__doc__.split('\n')[0],
    long_description=__doc__,
    # long_description_content_type='text/x-rst',

    # -- Requirements
    python_requires='>=3.5',
    # install_requires=[],
    # extras_require={
    #     'test': [  # install these with: pip install booster[test]
    #         "pytest>=3.8"
    #         "tox>=3.3",
    #     ],
    # },

    # -- Packaging
    # packages=find_packages(),
    include_package_data=True,
    # zip_safe=False,

    # -- Tests
    # test_suite='tests',

    # -- CLI
    # entry_points={
    #     "console_scripts": ['task=module:func']
    # },
    scripts=['bin/task.py', 'bin/taskobj.py'],

    # -- Metadata
    keywords="tracker organization",
    # project_urls={
    #     'Documentation': '',
    #     'Tracker': '',
    # },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: MIT License",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "Topic :: System :: Operating System",
        "Natural Language :: English",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)

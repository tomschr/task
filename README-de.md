# task - Ein Tool zum Verwalten von Aufgaben

## Features

Das Skript `task.py` kann:

* Anlegen von Aufgaben
* Auflisten von vorhandene Aufgaben
* Ändern vorhandener Aufgaben


## Syntax

```
task ACTION [OPTIONS]

ACTION = {create,list,edit}
```


## Aufruf

* Anlegen von neuen Aufgaben:

        $ task create python "Write Python script"

* Auflisten aller gespeicherter Aufgaben:

        $ task list

* Auflisten aller gespeicherter Aufgaben die das Schlüsselwort `python` enthält:

        $ task list python

* Ändern einer vorhandener Aufgabe:

        $ task edit ...


## Design

Folgende Entscheidungen über das Design des Tools sind
bekannt:

* Aufgaben werden im Ordner `~/.config/tasks/tasks` gespeichert.
* Der Dateiname jeder Aufgabe besteht aus:

        YYYY-MM-DDTHH:mm:SS.task

  Die Bedeutung der Variablen sind:

   * `YYYY`: ist das vierstellige Jahr
   * `MM`: Monat (01..12)
   * `DD`: Tag (01..31)
   * T: Trenner zwischen Datum und Uhrzeit
   * `mm`: Minuten (00..59)
   * `ss`: Sekunden (00..59)

* Der Inhalt jeder Aufgabe enthält:
   * Zeile mit Tags getrennt mit Kommas
   * Beschreibung der Aufgabe
* sonstiges
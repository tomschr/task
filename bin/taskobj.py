#!/usr/bin/env python3
"""
task is a small command line utility to create, list, remove, and edit tasks.
"""

import argparse
# from contextlib import contextmanager
import datetime
import functools
import re
from pathlib import Path
import sys


__author__ = "Thomas Schraitle <toms@suse.de>"
__version__ = "0.1.0"

PROG = Path(sys.argv[0]).name

# Not for Python 3.4
if hasattr(Path, 'home'):
    HOME = Path.home()
else:
    import os
    HOME = Path(os.path.expanduser('~'))


class Task(object):
    """A Task object

    Examples:
    >>> t = Task('2018-01-27T22:08:33.651492.task')
    >>> t.name
    '2018-01-27T22:08:33.651492'
    >>> Task.is_date_valid('2018-01-27T22:09:01.091986')
    True
    >>> Task.is_date_valid('foo')
    False
    >>> t = Task.create('2018-01-27T22:00:00.1')
    >>> t.name
    '2018-01-27T22:00:00.1'
    >>> Task("not-valid")
    Traceback (most recent call last):
    ...
    ValueError: Task filename 'not-valid' is not in ISO format
    ...
    """
    # Difference between classmethod and staticmethod,
    # see https://stackoverflow.com/a/12179752

    TASK_HOMEDIR = HOME / '.config/task/'
    TASKS_DIR = TASK_HOMEDIR / 'tasks'
    TASK_SUFFIX = '.task'
    TASK_FILE_PATTERN = "*{}".format(TASK_SUFFIX)

    SYNOPSIS = "YYYY-MM-DDThh:mm:ss[.ms]?[Z[+-]TIMEZONE]?"
    ISORE = re.compile(r'^(?P<year>-?(?:[1-9][0-9]*)?[0-9]{4})-'
                       r'(?P<month>1[0-2]|0[1-9])-'
                       r'(?P<day>3[01]|0[1-9]|[12][0-9])T'
                       r'(?P<hour>2[0-3]|[01][0-9]):'
                       r'(?P<minute>[0-5][0-9]):'
                       r'(?P<second>[0-5][0-9])'
                       r'(?P<ms>\.[0-9]+)?'
                       r'(?P<timezone>Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$')

    def __init__(self, filename):
        """Initialize a task with a filename

        :param str filename: a filename
        :raises: :class:`ValueError`
        """
        self.filename = filename
        # The following attributes are initialized in a lazy way
        self.title = None
        self.tags = None
        self.msg = None

        if not Task.TASKS_DIR.exists():
            Task.TASKS_DIR.mkdir(parents=True)

        if not Task.is_date_valid(self.name):
            raise ValueError("Task filename {!r} is not in ISO format\n"
                             "Expected {}".format(filename, Task.SYNOPSIS))

    # --- Properties
    @property
    def filename(self):
        """Getter for the Task filename"""
        return self.__filename

    @filename.setter
    def filename(self, fn):
        """Setter for filename

        :param pathlib.Path fn: path to Task file
        """
        fn = Path(fn)
        if not fn.is_absolute():
            self.__filename = Task.TASKS_DIR / fn
        else:
            self.__filename = fn

    @property
    def name(self):
        """Name of the .task filename without any path and extension
        """
        return self.filename.stem

    # --- class and static methods
    @classmethod
    def full_task_file(cls, base):
        """Returns the full task filename from a given base name

        :param str base: the base name in the format of a ISO date string
        :return: the full task name
        :rtype: str
        """
        return cls.TASKS_DIR / ("{}{}".format(base, cls.TASK_SUFFIX))

    @classmethod
    def create(cls, isodate=None):
        """Factory method to create a new Task object from an ISO date string

        :param cls: the class, usually :class:`Task` or any derived class
        :param str isodate: the string in ISO date format
        :return: A :class:`Task` object with the appropriate filename
        """
        # if isodate is None:
        #    base = datetime.datetime.now().isoformat()
        # else:
        #    base = isodate
        base = datetime.datetime.now().isoformat() \
               if isodate is None else isodate

        taskfile = cls.full_task_file(base)
        return cls(taskfile)

    @staticmethod
    def iter_task_files():
        """Yields a Task object from the database

        :yields: :class:`Task` object
        """
        taskfiles = (Task(taskfile)
                     for taskfile in Task.TASKS_DIR.glob(Task.TASK_FILE_PATTERN)
                     if taskfile.is_file()
                     )
        yield from taskfiles

    @staticmethod
    def is_date_valid(isodate):
        """Check for valid ISO date

        :param str isodate: the string in ISO date format
        :return: boolean value, True if the string is in ISO format,
                 False otherwise
        """
        # See https://www.safaribooksonline.com/library/view/regular-expressions-cookbook/9781449327453/ch04s07.html
        return bool(Task.ISORE.search(isodate))

    @staticmethod
    def remove(isodate):
        """Removes a taskfile from the list

        :param str isodate: the string in ISO date format
        :raise: FileNotFoundError if the file is not there
        """
        taskfile = Task.full_task_file(isodate)

        if taskfile.exists():
            taskfile.unlink()
            print("Task {!r} deleted.".format(isodate))
        else:
            raise FileNotFoundError("Cannot find task file "
                                    "{!r}.".format(taskfile))

    # --- Methods
    def read(self):
        """Read in a Task object and yield title, tags, and message"""
        with self.filename.open('r+t') as fh:
            self.title = fh.readline().strip()
            self.tags = fh.readline().strip()  # .split(",")
            self.msg = fh.read()

    def write(self, title, tags, msg):
        """Write a task

        :param title: the title of the task
        :param tags: available tags, separated by comma
        :param msg the message of the task
        """
        data = [title+"\n",
                '\n' if tags is None else tags+"\n",
                '\n' if msg is None else msg
                ]
        with self.filename.open("w+t") as fh:
            fh.writelines(data)

    # --- dunder methods
    def __repr__(self):
        """String representation of a Task object"""
        # return "{}({!r})".format(type(self).__name__, self.name)
        return "{}({self.name})".format(type(self).__name__, self=self)

    def __iter__(self):
        """Iterator for Task

        :yield: filename, title, and tags
        """
        yield from (self.filename, self.title, self.tags)

    def __format__(self, spec=''):
        """Format a Task object

        :param str spec: The format specification. Available codes:
          * filename = the base filename of the Task object
          * title    = the title
          * tags     = the available tags
          * msg      = the message (truncated to 20 characters)
          * indent   = indendation string (usually spaces)
        :return: the formatted Task object
        :rtype: str
        """
        # Our spec codes which maps to the given data:
        codes = {"filename": self.name,
                 "title": self.title,
                 "tags": self.tags,
                 "msg": self.msg,
                 "indent": "\n     ",
                 }

        # print(">> spec=", spec)
        if not spec:
            # Use default spec in case spec is empty:
            spec = "{filename}{indent}{title}{indent}{tags}{indent}{msg:.20}"
        try:
            result = spec.format(**codes)
        except KeyError as error:
            raise KeyError("Unknown identifier. "
                           "Expected: {}".format(", ".join(codes.keys())),
                           error)
        return result


# ------------------------------------------------------------
# Subcommands
# ------------------------------------------------------------

def cmd_create(args):
    """Create a task

    A tasks consists of the following lines:

    * First line: title
    * Second line: tags (comma separated)
    * Third-n lines: description

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    """
    print("Creating task:")
    title = getattr(args, 'title')
    tags = getattr(args, 'tags')
    msg = getattr(args, 'msg')
    # print("  with {title}, {tags} and {msg}".format(**locals()))
    task = Task.create()
    task.write(title=title,
               tags=tags,
               msg=msg
               )
    print("Wrote {}".format(task.name))


def cmd_list(args):
    """List all tasks

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    """
    print("Available Tasks:")
    for idx, task in enumerate(Task.iter_task_files(), 1):
        # We read in all the content of this task
        task.read()

        # If you don't like the default output of Task, you need to pass
        # a different format string. The format string has the syntax:
        #
        #  {:IDENTIFIERS}
        #
        # Whereas IDENTIFIERS is a placeholder for filename, title, or tags.
        # These identifiers need to be "masked" with double brackets to avoid
        # interpretion by the .format() method.
        #
        # Examples:
        # * "{}".format(task)
        #   default output as definied in Task.__format__
        # * "{:}".format(task)
        #   same as before
        # * "{:{{filename}}}".format(task)
        #   Print only the filename (e.g. date)
        # * "{:{{filename}}: {{title}}}".format(task)
        #   Print filename and title on the same line
        # * "{:{{filename}}: {{title:.<30}}}".format(task)
        #   Print filename and title as before, but fill title with dots
        #
        # See https://pyformat.info for more examples of string formatting
        print("{: 3d}: {}".format(idx, task))


def cmd_edit(args):
    """Edit a task

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    """
    # print(vars(args))
    if args.title is None and args.tags is None and args.msg is None:
        args.parser.error(
            "edit: Need one or all of --title, --tag, or --message")

    task = Task.create(args.date)
    if not task.filename.exists():
        args.parser.error("Date {} is not available".format(args.date))

    task.read()
    task.write(args.title or task.title,
               args.tags or task.tags,
               args.msg or task.msg
               )


def cmd_remove(args):
    """Removes one or more tasks

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    :raises: :class:`FileNotFoundError`
    """
    for date in args.date:
        Task.remove(date)


# ------------------------------------------------------------
def parse_cli(args=None):
    """Parse command line arguments

    :param list args: a list of arguments (basically sys.args[:1])
    :return: :class:'argparse.Namespace'
    """
    parser = argparse.ArgumentParser(prog=PROG,
                                     description=__doc__)
    parser.add_argument('--version',
                        action='version',
                        version=__version__)
    parser.add_argument('--verbose', '-v',
                        action='count',
                        help="increase verbosity level")
    subparsers = parser.add_subparsers(title='Subcommands',
                                       metavar='action',
                                       # dest='action',
                                       help="Available actions")

    # subparser for the "create" subcommand:
    # Syntax: create TITLE [TAG] MESSAGE
    pcreate = subparsers.add_parser('create',
                                    aliases=['c'],
                                    description=('Create a task with a title, '
                                                 'an optional tag and a '
                                                 'description'),
                                    help='Create a task')
    pcreate.set_defaults(action="create", func=cmd_create)
    pcreate.add_argument('title',
                         help="A short summary of this task")
    pcreate.add_argument('tags',
                         nargs='?',
                         default=None,
                         help='one or more optional tags for this task')
    pcreate.add_argument('msg',
                         metavar='message',
                         help='a short description of this task')

    # subparser of the "list" subcommand:
    # Syntax: list [TAG]
    plist = subparsers.add_parser('list',
                                  aliases=['l', 'li'],
                                  help='List tasks')
    plist.set_defaults(action="list", func=cmd_list)
    plist.add_argument('tag',
                       nargs='?',
                       help='the optional tag for this task')

    # subparser for the "edit" subcommand:
    # Syntax: edit
    pedit = subparsers.add_parser('edit',
                                  aliases=['e', 'ed'],
                                  help='Edit a task')
    pedit.set_defaults(action="edit", func=cmd_edit)
    pedit.add_argument('date',
                       help='the tasks\' date')
    pedit.add_argument('--title', '-t',
                       help='The title to change of this task')
    pedit.add_argument('--tags',
                       help='The tags to change for this task')
    pedit.add_argument('--message', '-m',
                       dest="msg",
                       help='The message to change for this task')

    # subparser for the "remove" subcommand:
    # Syntax: remove DATE [DATE ...]
    prm = subparsers.add_parser('remove',
                                aliases=['r', 'rm'],
                                help='Removes a task')
    prm.set_defaults(action="remove", func=cmd_remove)
    prm.add_argument('date',
                     nargs="+",
                     help='Removes the task with the specified date')

    # Parse the command line:
    args = parser.parse_args(args)
    # Save our parser object:
    args.parser = parser
    # print(">>>> args:", args)
    # If no argument is given, we print the help:
    if not sys.argv[1:]:
        parser.print_help()
        sys.exit(10)

    # We prepare our function with our parsed arguments
    args.func = functools.partial(args.func, args)

    return args


def main(args=None):
    """main function of the script

    :param list args: a list of arguments (basically sys.args[:1])
    :return: return code value
    :rtype: int
    """
    # Parse command line
    args = parse_cli(args)

    # Call the respective functions:
    result = 0
    try:
        args.func()
    except FileNotFoundError as error:
        print(error, file=sys.stderr)
        result = 10

    return result


# ------------------------------------------------------------
# Library or script check
# ------------------------------------------------------------

if __name__ == "__main__":
    sys.exit(main())

#!/usr/bin/env python3
"""
task is a small command line utility to create, list, remove, and edit tasks.
"""

# TODOS/TASKS:
# * Add a global option --config-dir to change the TASKS_DIR variable
# * Add a show subcommand with the signature "show [NUMBER|ISODATE]"
#   - if NUMBER or ISODATE is omitted, it outputs the latest task with
#     the complete contents
#   - if a number is given, it returns the contents of the task at the
#     given position
#   - if an ISODATE is given it tries to approximate the date to the
#     closest task

import argparse
import datetime
import functools
import glob
import os
import sys
import tempfile


__author__ = "Thomas Schraitle <toms@suse.de>"
__version__ = "0.1.0"

PROG = os.path.basename(sys.argv[0])
TASK_HOMEDIR = os.path.expandvars("$HOME/.config/task/")
TASKS_DIR = os.path.join(TASK_HOMEDIR, "tasks")
TASK_SUFFIX = '.task'
TASK_FILE_PATTERN = "*%s" % TASK_SUFFIX


# ------------------------------------------------------------
# Helper functions
# ------------------------------------------------------------
def taskfiles(taskdir=TASKS_DIR):
    """Yields all task file in the tasks folder

    :param taskdir: directory to search for task files
    :returns: tuple of filename and base name without extension
    :rtype: tuple(str, str)
    """
    for taskfile in glob.glob(os.path.join(TASKS_DIR, TASK_FILE_PATTERN)):
        basename = os.path.basename(taskfile)
        # Filter files which we don't want:
        if os.path.isfile(taskfile) or not basename.startswith('.'):
            # Remove file extension:
            base = os.path.splitext(basename)[0]
            # Check if the base name follows our ISO format
            # TODO:

            yield (taskfile, base)


# ------------------------------------------------------------
# Subcommands
# ------------------------------------------------------------

def cmd_create(args):
    """Create a task

    A tasks consists of the following lines:

    * First line: title
    * Second line: tags (comma separated)
    * Third-n lines: description

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    """
    print("Creating task:")
    base = "%s%s" % (datetime.datetime.now().isoformat(), TASK_SUFFIX)
    taskfile = os.path.join(TASKS_DIR, base)
    with open(taskfile, "w") as fh:
        for key in ('title', 'tag', 'msg'):
            data = getattr(args, key)
            if data is not None:
                print("%7s: %s" % (key, data))
                fh.write("%s\n" % data)

    print("Wrote %s" % base)


def cmd_list(args):
    """List all tasks

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    """
    print("Available Tasks:")
    for idx, task in enumerate(taskfiles(), 1):
        path, base = task
        with open(path, 'r') as fh:
            title = fh.readline().strip()
            tags = fh.readline().strip().split(",")
        print("%4d: %s\n"
              "      %s\n"
              "      %s\n" % (idx, base, title, tags))


def cmd_edit(args):
    """Edit a task

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    :return: success (=0) or error (!=0)
    """
    # print(vars(args))
    if args.title is None and args.tag is None and args.message is None:
        args.parser.error("edit: Need one or all of "
                          "--title, --tag, or --message")

    base = args.date
    taskfile = os.path.join(TASKS_DIR, "%s%s" % (base, TASK_SUFFIX))
    with tempfile.NamedTemporaryFile(encoding="UTF-8",
                                     mode='w+t',
                                     delete=False,
                                     prefix=".%s-" % base,
                                     suffix=TASK_SUFFIX,
                                     dir=TASKS_DIR,
                                     ) as tmp, \
            open(taskfile, 'r+t') as fh:
        for key in ('title', 'tag'):
            line = fh.readline()
            data = getattr(args, key)
            line = "%s\n" % data if data else line
            tmp.write(line)
        # Get and write the description
        for line in fh:
            tmp.write(line)

    # Now, we have a temporary file and still the original file
    # We need to rename/move the temporary file to the original file
    os.rename(tmp.name, taskfile)
    tmp.close()


def cmd_remove(args):
    """Removes a task

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    :return: success (=0) or error (!=0)
    """
    taskfile = os.path.join(TASKS_DIR, "%s%s" % (args.date, TASK_SUFFIX))
    if os.path.exists(taskfile):
        os.remove(taskfile)
    else:
        raise FileNotFoundError("Cannot find task file %r." % taskfile)


def cmd_show(args):
    """Shows the content of a specific task
    """
    base = "%s%s" % (args.date, TASK_SUFFIX)
    taskfile = os.path.join(TASKS_DIR, base)
    data = ('Title', 'Tag', 'Message')
    with open(taskfile, 'r+t') as fh:
        print("Reading {}".format(base))
        for line, d in zip(fh, data):
            line = line.strip()
            print("{:>10s}: {}".format(d, line))
    return 0


def cmd_help(args):
    """Help of subcommands

    :param args: parsed command line
    :type args: :class:'argparse.Namespace'
    :return: success (=0) or error (!=0)
    """
    parser = args.parser
    if args.cmd is None:
        parser.print_help()

    parser.parse_args([args.cmd, '-h'])


# ------------------------------------------------------------
def parse_cli(args=None):
    """Parse command line arguments

    :param list args: a list of arguments (basically sys.args[:1])
    :return: :class:'argparse.Namespace'
    """
    parser = argparse.ArgumentParser(prog=PROG,
                                     description=__doc__)
    parser.add_argument('--version',
                        action='version',
                        version=__version__)
    parser.add_argument('--verbose', '-v',
                        action='count',
                        help="increase verbosity level")
    subparsers = parser.add_subparsers(title='Subcommands',
                                       metavar='action',
                                       # dest='action',
                                       help="Available actions")

    # subparser for the "create" subcommand:
    # Syntax: create TITLE [TAG] MESSAGE
    pcreate = subparsers.add_parser('create',
                                    aliases=['c'],
                                    description=('Create a task with a title, '
                                                 'an optional tag and a description'),
                                    help='Create a task')
    pcreate.set_defaults(action="create", func=cmd_create)
    pcreate.add_argument('title',
                         help="A short summary of this task")
    pcreate.add_argument('tag',
                         nargs='?',
                         default=None,
                         help='the optional tag for this task')
    pcreate.add_argument('msg',
                         metavar='message',
                         help='a short description of this task')

    # subparser of the "list" subcommand:
    # Syntax: list [TAG]
    plist = subparsers.add_parser('list',
                                  aliases=['l', 'li'],
                                  help='List tasks')
    plist.set_defaults(action="list", func=cmd_list)
    plist.add_argument('tag',
                       nargs='?',
                       help='the optional tag for this task')

    # subparser for the "edit" subcommand:
    # Syntax: edit
    pedit = subparsers.add_parser('edit',
                                  aliases=['e', 'ed'],
                                  help='Edit a task')
    pedit.set_defaults(action="edit", func=cmd_edit)
    pedit.add_argument('date',
                       help='the tasks\' date')
    pedit.add_argument('--title', '-t',
                       help='The title to change of this task')
    pedit.add_argument('--tag',
                       help='The tag to change for this task')
    pedit.add_argument('--message', '-m',
                       help='The message to change for this task')

    # subparser for the "remove" subcommand:
    # Syntax: remove DATE
    prm = subparsers.add_parser('remove',
                                aliases=['r', 'rm'],
                                help='Removes a task')
    prm.set_defaults(action="remove", func=cmd_remove)
    prm.add_argument('date',
                     help='Removes the task with the specified date')

    # subparser for the "show" subcommand:
    # Syntax: show DATE
    pshow = subparsers.add_parser('show',
                                aliases=['s'],
                                help='Shows a task')
    pshow.set_defaults(subcmd="show",
                       func=cmd_show,
                       )
    pshow.add_argument('date',
                       help='Shows the task of the specified date')

    #
    #
    phelp = subparsers.add_parser('help',
                                  aliases=['h', '?'],
                                  help="Help of subcommands"
                                  )
    phelp.set_defaults(action="help", func=cmd_help)
    phelp.add_argument('cmd',
                       nargs='?',
                       help="Subcommand to get help")

    # Parse the command line:
    args = parser.parse_args(args)
    # Save our parser object:
    args.parser = parser
    # If no argument is given, we print the help:
    if not sys.argv[1:]:
        parser.print_help()
        sys.exit(0)

    # We prepare our function with our parsed arguments
    args.func = functools.partial(args.func, args)

    return args


def main(args=None):
    """main function of the script

    :param list args: a list of arguments (basically sys.args[:1])
    """
    # Parse command line
    args = parse_cli(args)

    result = 0
    # Call the respective functions:
    try:
        if not os.path.exists(TASKS_DIR):
            os.makedirs(TASKS_DIR)
        args.func()
    except FileNotFoundError as error:
        print(error, file=sys.stderr)
        result = 10

    return result


# ------------------------------------------------------------
# Library or script check
# ------------------------------------------------------------

if __name__ == "__main__":
    sys.exit(main())

#!/usr/bin/env python3
"""
task is a small command line utility to create, list, remove, and edit tasks.
"""

# TODOS/TASKS:
# - add the code

import argparse
import datetime
import functools
import glob
import os
import sys
import tempfile


__author__ = "Thomas Schraitle <toms@suse.de>"
__version__ = "0.1.0"

PROG = os.path.basename(sys.argv[0])
TASK_HOMEDIR = os.path.expandvars("$HOME/.config/task/")
TASKS_DIR = os.path.join(TASK_HOMEDIR, "tasks")
TASK_SUFFIX = '.task'
TASK_FILE_PATTERN = "*%s" % TASK_SUFFIX



# ------------------------------------------------------------

def cmd_create(args):
    """
    """
    print("** create")
    return 0


def cmd_list(args):
    """
    """
    print("* list")
    return 0


def cmd_edit(args):
    """
    """
    print("* edit")
    return 0


def cmd_remove(args):
    """
    """
    print("* remove")
    args.parser.error("Oh now, it's Monday!")
    return 0


def cmd_help(args):
    """Shows the help of an optional subcommand
    """
    if args.cmd is None:
        args.parser.print_help()
        return 0
    args.parser.parse_args([args.cmd, "-h"])
    return 0


def cmd_show(args):
    """Shows the help of an optional subcommand
    """
    print("** show")
    return 0


def parse_cli(args=None):
    """Parse command line arguments

    :param list args: a list of arguments (basically sys.args[:1])
    :return: :class:'argparse.Namespace'
    """
    parser = argparse.ArgumentParser(prog=PROG,
                                     description=__doc__)
    parser.add_argument('--version',
                        action='version',
                        version=__version__)
    parser.add_argument('--verbose', '-v', action='count')
    subparsers = parser.add_subparsers(title='Subcommands',
                                       metavar='Subcommands',
                                       dest='subcmd',
                                       help="Available actions")

    # subparser for the "create" subcommand:
    # Syntax: create TITLE [TAG] MESSAGE
    pcreate = subparsers.add_parser('create',
                                    aliases=['c'],
                                    description=('Create a task with a title, '
                                                 'an optional tag and a description'),
                                    help='Create a task')
    pcreate.set_defaults(subcmd="create",
                         func=cmd_create,
                         )
    pcreate.add_argument('title',
                         help="A short summary of this task")
    pcreate.add_argument('tag',
                         nargs='?',
                         default=None,
                         help='the optional tag for this task')
    pcreate.add_argument('msg',
                         metavar='MESSAGE',
                         help='a short description of this task')

    # subparser of the "list" subcommand:
    # Syntax: list [TAG]
    plist = subparsers.add_parser('list',
                                  aliases=['l', 'li'],
                                  help='List tasks')
    plist.set_defaults(subcmd="list",
                       func=cmd_list,
                       )
    plist.add_argument('tag',
                       nargs='?',
                       help='the optional tag for this task')

    # subparser for the "edit" subcommand:
    # Syntax: edit
    pedit = subparsers.add_parser('edit',
                                  aliases=['e', 'ed'],
                                  help='Edit a task')
    pedit.set_defaults(subcmd="edit",
                       func=cmd_edit,
                       )
    pedit.add_argument('date',
                       help='the tasks\' date')
    pedit.add_argument('--title', '-t',
                       help='The title to change of this task')
    pedit.add_argument('--tag',
                       help='The tag to change for this task')
    pedit.add_argument('--message', '-m',
                       help='The message to change for this task')

    # subparser for the "remove" subcommand:
    # Syntax: remove DATE
    prm = subparsers.add_parser('remove',
                                aliases=['r', 'rm'],
                                help='Removes a task')
    prm.set_defaults(subcmd="remove",
                     func=cmd_remove,
                       )
    prm.add_argument('date',
                     help='Removes the task with the specified date')

    # subparser for the "show" subcommand:
    # Syntax: show DATE
    pshow = subparsers.add_parser('show',
                                aliases=['s'],
                                help='Shows a task')
    pshow.set_defaults(subcmd="show",
                       func=cmd_show,
                       )
    pshow.add_argument('date',
                       help='Shows the task of the specified date')

    # subparser for the "help" subcommand
    # Syntax: help [SUBCOMMAND]
    phelp = subparsers.add_parser('help',
                                  aliases=['h', '?'],
                                  help="Help of subcommands"
                                  )
    phelp.set_defaults(subcmd="help",
                       func=cmd_help,
                       )
    phelp.add_argument('cmd',
                       nargs='?',
                       help="Subcommand to get help")

    # Parse the command line:
    args = parser.parse_args(args)
    # Save our parser object:
    args.parser = parser
    
    # If no argument is given, we print the help:
    if not sys.argv[1:]:
        parser.print_help()
        sys.exit(0)

    return args


def main(args=None):
    """main function of the script

    :param list args: a list of arguments (basically sys.args[:1])
    """
    # Parse command line
    args = parse_cli(args)
    print("Calling subcommand", args.subcmd)
    print(args)

    result = args.func(args)

    return result


# ------------------------------------------------------------
# Library or script check
# ------------------------------------------------------------

if __name__ == "__main__":
    sys.exit(main())
